package com.joshparker.romannumeralconverter.factory;

import com.joshparker.romannumeralconverter.converter.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RomanNumeralConverterFactory {
    private static final int PLACE_VALUE_MAX_UNIT = 9;
    private static final int PLACE_VALUE_MAX_TENS = 99;
    private static final int PLACE_VALUE_MAX_HUNDREDS = 999;
    private static final int PLACE_VALUE_MAX_THOUSANDS = 9999;
    private static final int PLACE_VALUE_MAX_TEN_THOUSANDS = 99999;
    private static final int PLACE_VALUE_MAX_HUNDRED_THOUSANDS = 999999;
    private static final int PLACE_VALUE_MAX_MILLIONS = 3999999;

    public static String getRomanNumeralValue(final int number) {
        StringBuilder romanNumeralValue = new StringBuilder();

        if (number <= PLACE_VALUE_MAX_UNIT) {
            return new UnitConverter().convertToRomanNumeral(number, UnitConverter.ROMAN_NUMERALS_UNITS);
        }

        if (number <= PLACE_VALUE_MAX_TENS) {
            List<Integer> placeValues = intToIntList(number);
            List<String> allPlaceValues = Arrays.asList(
                    new TensConverter().convertToRomanNumeral(placeValues.get(0), TensConverter.ROMAN_NUMERALS_TENS),
                    new UnitConverter().convertToRomanNumeral(placeValues.get(1), UnitConverter.ROMAN_NUMERALS_UNITS)
            );
            allPlaceValues.forEach(romanNumeralValue::append);
            return romanNumeralValue.toString();
        }

        if (number <= PLACE_VALUE_MAX_HUNDREDS) {
            List<Integer> placeValues = intToIntList(number);
            List<String> allPlaceValues = Arrays.asList(
                    new HundredsConverter().convertToRomanNumeral(placeValues.get(0), HundredsConverter.ROMAN_NUMERALS_HUNDREDS),
                    new TensConverter().convertToRomanNumeral(placeValues.get(1), TensConverter.ROMAN_NUMERALS_TENS),
                    new UnitConverter().convertToRomanNumeral(placeValues.get(2), UnitConverter.ROMAN_NUMERALS_UNITS)
            );
            allPlaceValues.forEach(romanNumeralValue::append);
            return romanNumeralValue.toString();
        }

        if (number <= PLACE_VALUE_MAX_THOUSANDS) {
            List<Integer> placeValues = intToIntList(number);
            List<String> allPlaceValues = Arrays.asList(
                    new ThousandsConverter().convertToRomanNumeral(placeValues.get(0), ThousandsConverter.ROMAN_NUMERALS_THOUSANDS),
                    new HundredsConverter().convertToRomanNumeral(placeValues.get(1), HundredsConverter.ROMAN_NUMERALS_HUNDREDS),
                    new TensConverter().convertToRomanNumeral(placeValues.get(2), TensConverter.ROMAN_NUMERALS_TENS),
                    new UnitConverter().convertToRomanNumeral(placeValues.get(3), UnitConverter.ROMAN_NUMERALS_UNITS)
            );
            allPlaceValues.forEach(romanNumeralValue::append);
            return romanNumeralValue.toString();
        }

        if (number <= PLACE_VALUE_MAX_TEN_THOUSANDS) {
            List<Integer> placeValues = intToIntList(number);
            List<String> allPlaceValues = Arrays.asList(
                new TenThousandsConverter().convertToRomanNumeral(placeValues.get(0), TenThousandsConverter.ROMAN_NUMERALS_TEN_THOUSANDS),
                new ThousandsConverter().convertToRomanNumeral(placeValues.get(1), ThousandsConverter.ROMAN_NUMERALS_THOUSANDS),
                new HundredsConverter().convertToRomanNumeral(placeValues.get(2), HundredsConverter.ROMAN_NUMERALS_HUNDREDS),
                new TensConverter().convertToRomanNumeral(placeValues.get(3), TensConverter.ROMAN_NUMERALS_TENS),
                new UnitConverter().convertToRomanNumeral(placeValues.get(4), UnitConverter.ROMAN_NUMERALS_UNITS)
            );
            allPlaceValues.forEach(romanNumeralValue::append);
            return romanNumeralValue.toString();
        }

        if (number <= PLACE_VALUE_MAX_HUNDRED_THOUSANDS) {
            List<Integer> placeValues = intToIntList(number);
            List<String> allPlaceValues = Arrays.asList(
                new HundredThousandsConverter().convertToRomanNumeral(placeValues.get(0), HundredThousandsConverter.ROMAN_NUMERALS_HUNDRED_THOUSANDS),
                new TenThousandsConverter().convertToRomanNumeral(placeValues.get(1), TenThousandsConverter.ROMAN_NUMERALS_TEN_THOUSANDS),
                new ThousandsConverter().convertToRomanNumeral(placeValues.get(2), ThousandsConverter.ROMAN_NUMERALS_THOUSANDS),
                new HundredsConverter().convertToRomanNumeral(placeValues.get(3), HundredsConverter.ROMAN_NUMERALS_HUNDREDS),
                new TensConverter().convertToRomanNumeral(placeValues.get(4), TensConverter.ROMAN_NUMERALS_TENS),
                new UnitConverter().convertToRomanNumeral(placeValues.get(5), UnitConverter.ROMAN_NUMERALS_UNITS)
            );
            allPlaceValues.forEach(romanNumeralValue::append);
            return romanNumeralValue.toString();
        }

        if (number <= PLACE_VALUE_MAX_MILLIONS) {
            List<Integer> placeValues = intToIntList(number);
            List<String> allPlaceValues = Arrays.asList(
                new MillionsConverter().convertToRomanNumeral(placeValues.get(0), MillionsConverter.ROMAN_NUMERALS_MILLIONS),
                new HundredThousandsConverter().convertToRomanNumeral(placeValues.get(1), HundredThousandsConverter.ROMAN_NUMERALS_HUNDRED_THOUSANDS),
                new TenThousandsConverter().convertToRomanNumeral(placeValues.get(2), TenThousandsConverter.ROMAN_NUMERALS_TEN_THOUSANDS),
                new ThousandsConverter().convertToRomanNumeral(placeValues.get(3), ThousandsConverter.ROMAN_NUMERALS_THOUSANDS),
                new HundredsConverter().convertToRomanNumeral(placeValues.get(4), HundredsConverter.ROMAN_NUMERALS_HUNDREDS),
                new TensConverter().convertToRomanNumeral(placeValues.get(5), TensConverter.ROMAN_NUMERALS_TENS),
                new UnitConverter().convertToRomanNumeral(placeValues.get(6), UnitConverter.ROMAN_NUMERALS_UNITS)
            );
            allPlaceValues.forEach(romanNumeralValue::append);
            return romanNumeralValue.toString();
        }

        return null;
    }

    private static List<Integer> intToIntList(int number) {
        String[] stringArr = Integer.toString(number).split("");
        List<Integer> numbers = new ArrayList<>();
        for (String s : stringArr) {
            numbers.add(Integer.parseInt(s));
        }
        return numbers;
    }

}
