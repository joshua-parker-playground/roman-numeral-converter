package com.joshparker.romannumeralconverter.prompter;

import java.util.Scanner;

public class Prompter {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static int askForNumber() {
        System.out.print("Enter a number between 1 and 3,999,999: ");
        return SCANNER.nextInt();
    }
}
