package com.joshparker.romannumeralconverter.model;

import com.joshparker.romannumeralconverter.factory.RomanNumeralConverterFactory;

public class RomanNumeral {
    private String symbolValue;

    public RomanNumeral(int number) {
        constructSymbolValue(number);
    }

    private void constructSymbolValue(int number) {
        this.symbolValue = RomanNumeralConverterFactory.getRomanNumeralValue(number);
    }

    public String getSymbolValue() {
        return this.symbolValue;
    }
}
