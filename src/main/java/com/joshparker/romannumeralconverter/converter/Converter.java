package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

public class Converter {

    public String convertToRomanNumeral(final int number, final Map<Integer, String> ROMAN_NUMERAL_PLACE_VALUES) {
        StringBuilder romanNumeral = new StringBuilder();

        if (number > 0 && number < 4) {
            for (int i = 0; i < number; i++) {
                romanNumeral.append(ROMAN_NUMERAL_PLACE_VALUES.get(1));
            }
        }

        else if (number == 4) {
            romanNumeral.append(ROMAN_NUMERAL_PLACE_VALUES.get(4));
        }

        else if (number == 5) {
            romanNumeral.append(ROMAN_NUMERAL_PLACE_VALUES.get(5));
        }

        else if (number > 5 && number < 9) {
            int lengthAfterFive = number - 5;
            romanNumeral.append(ROMAN_NUMERAL_PLACE_VALUES.get(5));
            for (int i = 0; i < lengthAfterFive; i++) {
                romanNumeral.append(ROMAN_NUMERAL_PLACE_VALUES.get(1));
            }
        }

        else if (number == 9) {
            romanNumeral.append(ROMAN_NUMERAL_PLACE_VALUES.get(9));
        }

        return romanNumeral.toString();
    }
}
