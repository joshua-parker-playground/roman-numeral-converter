package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class HundredThousandsConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_HUNDRED_THOUSANDS = Map.ofEntries(
            entry(1, "_C"),
            entry(4, "_C_D"),
            entry(5, "_D"),
            entry(9, "_C_M")
    );
}
