package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class UnitConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_UNITS = Map.ofEntries(
            entry(1, "I"),
            entry(4, "IV"),
            entry(5, "V"),
            entry(9, "IX")
    );
}
