package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class TenThousandsConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_TEN_THOUSANDS = Map.ofEntries(
            entry(1, "_X"),
            entry(4, "_X_L"),
            entry(5, "_L"),
            entry(9, "_X_C")
    );
}
