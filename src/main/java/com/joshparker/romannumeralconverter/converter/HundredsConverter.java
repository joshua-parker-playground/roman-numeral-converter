package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class HundredsConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_HUNDREDS = Map.ofEntries(
            entry(1, "C"),
            entry(4, "CD"),
            entry(5, "D"),
            entry(9, "CM")
    );
}
