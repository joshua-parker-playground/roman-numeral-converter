package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class MillionsConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_MILLIONS = Map.ofEntries(
            entry(1, "_M")
    );
}
