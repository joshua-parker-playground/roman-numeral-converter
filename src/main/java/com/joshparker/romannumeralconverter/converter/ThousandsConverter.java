package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class ThousandsConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_THOUSANDS = Map.ofEntries(
            entry(1, "M"),
            entry(4, "_I_V"),
            entry(5, "_V"),
            entry(9, "_I_X")
    );
}
