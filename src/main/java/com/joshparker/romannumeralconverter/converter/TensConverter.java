package com.joshparker.romannumeralconverter.converter;

import java.util.Map;

import static java.util.Map.entry;

public class TensConverter extends Converter {

    public static final Map<Integer, String> ROMAN_NUMERALS_TENS = Map.ofEntries(
            entry(1, "X"),
            entry(4, "XL"),
            entry(5, "L"),
            entry(9, "XC")
    );
}
