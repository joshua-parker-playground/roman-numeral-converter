package com.joshparker.romannumeralconverter;

import com.joshparker.romannumeralconverter.model.RomanNumeral;
import com.joshparker.romannumeralconverter.prompter.Prompter;

import java.util.Scanner;

public class RomanNumeralConverter {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int number = Prompter.askForNumber();

        RomanNumeral romanNumeral = new RomanNumeral(number);
        System.out.println(romanNumeral.getSymbolValue());
    }
}
