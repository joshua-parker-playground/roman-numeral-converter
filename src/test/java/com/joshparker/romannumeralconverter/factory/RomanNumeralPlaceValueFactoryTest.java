//package com.joshparker.romannumeralconverter.factory;
//
//import com.joshparker.romannumeralconverter.converter.Converter;
//import com.joshparker.romannumeralconverter.converter.UnitConverter;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//
//import java.util.stream.Stream;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.junit.jupiter.params.provider.Arguments.arguments;
//
//class RomanNumeralPlaceValueFactoryTest {
//
//    @Mock
//    private Converter unitConverter;
//
//    @DisplayName("GIVEN valid number")
//    @ParameterizedTest(name = "{0} converts to {1}")
//    @MethodSource("testCasesProvided")
//    void getPlaceValues(int input, String expectedValue) {
//        this.unitConverter = new UnitConverter();
//
//        Mockito.when(unitConverter.convertToRomanNumeral())
//    }
//
//    static Stream<Arguments> testCasesProvided() {
//        return Stream.of(
//                arguments(1, "I"),
//                arguments(2, "II"),
//                arguments(3, "III"),
//                arguments(4, "IV"),
//                arguments(5, "V"),
//                arguments(6, "VI"),
//                arguments(7, "VII"),
//                arguments(8, "VIII"),
//                arguments(9, "IX")
//        );
//    }
//}