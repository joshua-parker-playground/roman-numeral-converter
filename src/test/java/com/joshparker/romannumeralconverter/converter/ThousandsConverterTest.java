package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ThousandsConverterTest {

    public static final Map<Integer, String> ROMAN_NUMERALS_THOUSANDS = Map.ofEntries(
            entry(1, "M"),
            entry(4, "_I_V"),
            entry(5, "_V"),
            entry(9, "_I_X")
    );

    private Converter thousandsConverter;

    @BeforeEach
    void setUp() {
        this.thousandsConverter = new ThousandsConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = thousandsConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_THOUSANDS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "M"),
                arguments(2, "MM"),
                arguments(3, "MMM"),
                arguments(4, "_I_V"),
                arguments(5, "_V"),
                arguments(6, "_VM"),
                arguments(7, "_VMM"),
                arguments(8, "_VMMM"),
                arguments(9, "_I_X")
        );
    }
}