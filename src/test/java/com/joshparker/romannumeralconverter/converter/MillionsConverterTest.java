package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class MillionsConverterTest {

    public static final Map<Integer, String> ROMAN_NUMERALS_MILLIONS = Map.ofEntries(
            entry(1, "_M")
    );

    private Converter millionsConverter;

    @BeforeEach
    void setUp() {
        this.millionsConverter = new MillionsConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = millionsConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_MILLIONS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "_M"),
                arguments(2, "_M_M"),
                arguments(3, "_M_M_M")
        );
    }
}