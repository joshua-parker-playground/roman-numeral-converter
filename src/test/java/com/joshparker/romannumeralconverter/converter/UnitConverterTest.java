package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class UnitConverterTest {

    public static final Map<Integer, String> ROMAN_NUMERALS_UNITS = Map.ofEntries(
            entry(1, "I"),
            entry(4, "IV"),
            entry(5, "V"),
            entry(9, "IX")
    );

    private Converter unitConverter;

    @BeforeEach
    void setUp() {
        this.unitConverter = new UnitConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = unitConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_UNITS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "I"),
                arguments(2, "II"),
                arguments(3, "III"),
                arguments(4, "IV"),
                arguments(5, "V"),
                arguments(6, "VI"),
                arguments(7, "VII"),
                arguments(8, "VIII"),
                arguments(9, "IX")
        );
    }
}