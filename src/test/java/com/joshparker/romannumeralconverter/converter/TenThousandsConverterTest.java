package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class TenThousandsConverterTest {

    private static final Map<Integer, String> ROMAN_NUMERALS_TEN_THOUSANDS = Map.ofEntries(
            entry(1, "_X"),
            entry(4, "_X_L"),
            entry(5, "_L"),
            entry(9, "_X_C")
    );

    private Converter tenThousandsConverter;

    @BeforeEach
    void setUp() {
        this.tenThousandsConverter = new TenThousandsConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = tenThousandsConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_TEN_THOUSANDS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "_X"),
                arguments(2, "_X_X"),
                arguments(3, "_X_X_X"),
                arguments(4, "_X_L"),
                arguments(5, "_L"),
                arguments(6, "_L_X"),
                arguments(7, "_L_X_X"),
                arguments(8, "_L_X_X_X"),
                arguments(9, "_X_C")
        );
    }
}