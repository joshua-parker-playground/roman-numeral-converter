package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class TensConverterTest {

    public static final Map<Integer, String> ROMAN_NUMERALS_TENS = Map.ofEntries(
            entry(1, "X"),
            entry(4, "XL"),
            entry(5, "L"),
            entry(9, "XC")
    );

    private Converter tensConverter;

    @BeforeEach
    void setUp() {
        this.tensConverter = new TensConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = tensConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_TENS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "X"),
                arguments(2, "XX"),
                arguments(3, "XXX"),
                arguments(4, "XL"),
                arguments(5, "L"),
                arguments(6, "LX"),
                arguments(7, "LXX"),
                arguments(8, "LXXX"),
                arguments(9, "XC")
        );
    }
}