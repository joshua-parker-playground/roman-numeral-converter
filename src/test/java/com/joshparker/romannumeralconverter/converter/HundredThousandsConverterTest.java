package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class HundredThousandsConverterTest {

    public static final Map<Integer, String> ROMAN_NUMERALS_HUNDRED_THOUSANDS = Map.ofEntries(
            entry(1, "_C"),
            entry(4, "_C_D"),
            entry(5, "_D"),
            entry(9, "_C_M")
    );

    private Converter hundredThousandsConverter;

    @BeforeEach
    void setUp() {
        this.hundredThousandsConverter = new HundredThousandsConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = hundredThousandsConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_HUNDRED_THOUSANDS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "_C"),
                arguments(2, "_C_C"),
                arguments(3, "_C_C_C"),
                arguments(4, "_C_D"),
                arguments(5, "_D"),
                arguments(6, "_D_C"),
                arguments(7, "_D_C_C"),
                arguments(8, "_D_C_C_C"),
                arguments(9, "_C_M")
        );
    }
}