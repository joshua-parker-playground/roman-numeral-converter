package com.joshparker.romannumeralconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class HundredsConverterTest {

    public static final Map<Integer, String> ROMAN_NUMERALS_HUNDREDS = Map.ofEntries(
            entry(1, "C"),
            entry(4, "CD"),
            entry(5, "D"),
            entry(9, "CM")
    );

    private Converter hundredsConverter;

    @BeforeEach
    void setUp() {
        this.hundredsConverter = new HundredsConverter();
    }

    @DisplayName("GIVEN valid number")
    @ParameterizedTest(name = "{0} converts to {1}")
    @MethodSource("testCasesProvided")
    void convertToRomanNumeral(int input, String expectedValue) {
        String response = hundredsConverter.convertToRomanNumeral(input, ROMAN_NUMERALS_HUNDREDS);
        assertEquals(expectedValue, response);
    }

    static Stream<Arguments> testCasesProvided() {
        return Stream.of(
                arguments(1, "C"),
                arguments(2, "CC"),
                arguments(3, "CCC"),
                arguments(4, "CD"),
                arguments(5, "D"),
                arguments(6, "DC"),
                arguments(7, "DCC"),
                arguments(8, "DCCC"),
                arguments(9, "CM")
        );
    }
}